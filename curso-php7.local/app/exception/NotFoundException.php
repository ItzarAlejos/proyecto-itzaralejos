<?php
namespace cursophp7\app\exception;


use Exception;

class NotFoundException extends AppException
{
    public function __construct($message,  $code = 404)
    {
        parent::__construct($message, $code);
    }
}