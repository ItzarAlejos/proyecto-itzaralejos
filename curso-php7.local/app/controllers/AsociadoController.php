<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 7/12/18
 * Time: 18:38
 */

namespace cursophp7\app\controllers;


use cursophp7\app\entity\Asociado;
use cursophp7\app\exception\AppException;
use cursophp7\app\exception\FileException;
use cursophp7\app\exception\QueryException;
use cursophp7\app\exception\ValidationException;
use cursophp7\app\repository\AsociadoRepository;
use cursophp7\app\utils\File;
use cursophp7\core\App;
use cursophp7\core\Response;

class AsociadoController
{
    /**
     * @throws QueryException
     */
    public function index()
    {
        $asociados=App::getRepository(AsociadoRepository::class)->findAll();
        Response::renderView('asociados','layout',
            compact('asociados')
        );
    }

    /**
     * @throws AppException
     * @throws QueryException
     */
    public function nuevo()
    {
        try{

            $nombre=trim(htmlspecialchars($_POST['nombre']));



            if(empty($nombre)){//SI EL NOMBRE ES VALIDO//REALIZO LAS
                throw new ValidationException("El nombre no puede quedar vacío");
            }
            $descripcion=trim(htmlspecialchars($_POST['descripcion']));

            $tiposAceptados=['image/jpeg','image/png','image/gif'];

            $imagen=new File('logo',$tiposAceptados);


            $imagen->saveUploadFile(Asociado::RUTA_IMAGEN_ASOCIADOS);
            $asociado=new Asociado($nombre,$imagen->getFileName(),$descripcion);



            App::getRepository(AsociadoRepository::class)->save($asociado);
            $message="Se ha guardado un nuevo asociado" . $asociado->getNombre();
            App::get('logger')->add($message);



        }catch(FileException $fileException){
                die($fileException->getMessage());

        }catch(ValidationException $validationException){

            die($validationException->getMessage());
        }


        App::get('router')->redirect('asociados');

    }
}