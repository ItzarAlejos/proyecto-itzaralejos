<?php

namespace cursophp7\app\repository;

use cursophp7\app\entity\Categoria;
use cursophp7\app\entity\ImageGallery;
use cursophp7\core\database\QueryBuilder;
use cursophp7\app\exceptions\NotFoundException;
use cursophp7\app\exceptions\QueryException;


class ImagenGaleriaRepository extends QueryBuilder
{

    /**
     * ImagenGaleriaRepository constructor.
     */
    public function __construct($table='Imagenes', $classEntity=ImageGallery::class)
    {
        parent::__construct($table,$classEntity);
    }

    /**
     * @param ImagenGaleria $imagenGaleria
     * @return Categoria
     * @throws NotFoundException
     * @throws QueryException
     */
    public function getCategoria(ImageGallery $imagenGaleria): Categoria
    {
        $categoriaRepository= new CategoriaRepository();

        return $categoriaRepository->find($imagenGaleria->getCategoria());
    }

    /**
     * @param ImageGallery $imageGallery
     * @throws QueryException
     */
    public function guarda(ImageGallery $imageGallery)
    {
        $fnGuardaImagen=function() use ($imageGallery)
        {
          $categoria=$this->getCategoria($imageGallery);
          $categoriaRepository=new CategoriaRepository();
          $categoriaRepository->nuevaImagen($categoria);
          $this->save($imageGallery);
        };
        $this->executeTransaction($fnGuardaImagen);
    }
}