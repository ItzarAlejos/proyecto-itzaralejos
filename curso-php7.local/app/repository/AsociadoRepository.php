<?php
namespace cursophp7\app\repository;

use cursophp7\app\entity\Asociado;
use cursophp7\core\database\QueryBuilder;


class AsociadoRepository extends QueryBuilder
{
    /**
     * AsociadoRepository constructor.
     * @param string $table
     * @param string $classEntity
     */
    public function __construct($table='Asociados', $classEntity=Asociado::class)
    {
        parent::__construct($table,$classEntity);
    }
}