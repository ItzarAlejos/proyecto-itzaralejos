<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 17/12/18
 * Time: 15:34
 */

namespace cursophp7\core;


use cursophp7\app\exception\AppException;

class Security
{
    /**
     * @param string $role
     * @return bool
     * @throws AppException
     */
    public static function isUserGranted(string $role)
    {

        if($role ==="ROLE_ANONYMOUS"){
            return true;
        }
        $usuario=App::get('appUser');
        if(is_null($usuario)){
            return false;
        }
        $valor_role=App::get('config')['security']['roles'][$role];
        $valor_role_usuario=App::get('config')['security']['roles'][$usuario->getRole()];

        return ($valor_role_usuario>=$valor_role);
    }

    public static function encrypt($password):string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    public static function checkPassword($password,$bdPassword):bool
    {
        return password_verify($password,$bdPassword);
    }
}