<?php

//require_once __DIR__ . '/../exception/QueryException.php';
//require_once __DIR__ . '/../core/App.php';

namespace cursophp7\core\database;


use cursophp7\app\exception\AppException;
use cursophp7\app\exception\NotFoundException;
use cursophp7\app\exception\QueryException;
use cursophp7\core\App;
use PDO;
use PDOException;

abstract class QueryBuilder
{
    /**
     * @var PDO
     */
    private $connection;
    /**
     * @var string
     */
    private $table;
    /**
     * @var string
     */
    private $classEntity;

    /**
     * QueryBuilder constructor.
     * @param string $table
     * @param string $classEntity
     * @throws AppException
     */
    public function __construct(string $table,string $classEntity)
    {
        $this->connection = App::getConnection();
        $this->table=$table;
        $this->classEntity=$classEntity;
    }

    /**
     * @return array
     * @throws QueryException
     */
    public function findAll() : array
    {
        $sql="SELECT * from $this->table";

        return $this->executeQuery($sql);
    }

    /**
     * @param string $sql
     * @return array
     * @throws QueryException
     */
    public function executeQuery(string $sql, array $parameters=[]): array
    {
        $pdoStatement=$this->connection->prepare($sql);


        if($pdoStatement->execute($parameters)===false){
            throw new QueryException("No se ha podido ejecutar la query solicitada ");
        }

        return $pdoStatement->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classEntity );
    }


    public function save(IEntity $entity) : void
    {
        try{

            $parameters=$entity->toArray();
            $sql=sprintf(
                'insert into %s (%s) values (%s)',
                $this->table,
                implode(', ',array_keys($parameters)),
                ':' .implode(', :', array_keys($parameters))
            );

            $statement=$this->connection->prepare($sql);

            $statement->execute($parameters);

        }catch(PDOException $exception){
            throw new QueryException('Error al insertar en la base de datos');
        }

    }

    /**
     * @param int $id
     * @return IEntity
     * @throws NotFoundException
     * @throws QueryException
     */
    public function find(int $id):IEntity
    {
        $sql="SELECT * from $this->table where id=$id";

        $result= $this->executeQuery($sql);

        if(empty($result)){
            throw new NotFoundException("No se ha encontrado ningún elemento con id $id");
        }

        return $result[0];
    }

    private function getFilters(array $filters)
    {
        if(empty($filters)){
            return '';
        }
        $strFilters=[];

        foreach($filters as $key=>$value){
            $strFilters[]=$key . '=:' . $key;
        }

        return ' WHERE '. implode(' and ',$strFilters);
    }
    public function findBy(array $filters):array
    {
        $sql="SELECT * from $this->table " . $this->getFilters($filters);

        return $this->executeQuery($sql,$filters);

    }

    public function findOneBy(array $filters)
    {
        $result=$this->findBy($filters);
        if(count($result)>0){
            return $result[0];
        }
        return null;
    }

    public function getUpdates(array $parameters)
    {
        $updates='';
        foreach($parameters as $key =>$value){
            if($key!=='id'){
                if($updates!==''){
                    $updates.=', ';
                }
                $updates.=$key .'=:' .$key;
            }
        }
        return $updates;
    }
    public function update(IEntity $entity):void
    {
        try{
            $parameters=$entity->toArray();

            $sql=sprintf(
                'UPDATE %s SET %s WHERE id=:id',
                $this->table,
                $this->getUpdates($parameters)
            );
            $statement=$this->connection->prepare($sql);

            $statement->execute($parameters);

        }
        catch (PDOException $pdoException){
            throw new QueryException('Error al actualizar el elemento con id' . $parameters['id']);
        }

    }

    /**
     * @param callable $fnExecuteQuerys
     * @throws QueryException
     */
    public function executeTransaction(callable $fnExecuteQuerys)
    {
        try{
            $this->connection->beginTransaction();
            $fnExecuteQuerys();

            $this->connection->commit();

        }catch(PDOException $pdoException){
            $this->connection->rollBack();

            throw new QueryException('No se ha podido realizar la operación');
        }
    }
}