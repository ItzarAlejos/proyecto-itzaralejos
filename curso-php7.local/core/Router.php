<?php
namespace cursophp7\core;
use cursophp7\app\exception\AppException;
use cursophp7\app\exception\AuthenticationException;
use cursophp7\app\exception\NotFoundException;

/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 4/12/18
 * Time: 20:13
 */

class Router
{
    private $routes;


    private function __construct()
    {
        $this->routes=[
            'GET'=>[],
            'POST'=>[]
        ];
    }

    public static function load( $file)
    {
        $router=new Router();
        require $file;
        return $router;
    }

    /**
     * @param string $uri
     * @param string $controller
     * @param string $role
     */
    public function get($uri, $controller, $role='ROLE_ANONYMOUS')
    {
        $this->routes['GET'][$uri]=[
            'controller'=>$controller,
            'role'=>$role
            ];
    }

    /**
     * @param string $uri
     * @param string $controller
     * @param string $role
     */
    public function post( $uri, $controller,$role='ROLE_ANONYMOUS')
    {
        $this->routes['POST'][$uri]=[
            'controller'=>$controller,
            'role'=>$role
        ];
    }

    /**
     * @param $controller
     * @param $action
     * @param $parameters
     * @return bool
     * @throws AppException
     * @throws NotFoundException
     */
    private function callAction($controller,$action, array $parameters):bool
    {
        try{
            $controller=App::get('config')['project']['namespace'].'\\app\\controllers\\' . $controller;
            $objController=new $controller();

            if(!method_exists($objController,$action)){
                throw new NotFoundException("El controlador $controller no responde al action $action");
            }

            call_user_func_array(array($objController,$action),$parameters);
            return true;


        }catch(\TypeError $typeError){
            return false;
        }

    }
    /**
     * @param $route
     * @return string
     */
    private function prepareRoute($route){
        $urlRule=preg_replace(
            '/:([^\/]+)/',
            '(?<\1>[^/]+)',
            $route
        );
        $urlRule=str_replace('/','\/',$urlRule);

        return '/^' . $urlRule . '\/*$/s';
    }

    /**
     * @param $route
     * @param $matches
     * @return array
     */
    private function getParametersRoute($route, array $matches){

        preg_match_all('/:([^\/]+)/', $route, $parameterNames);
        $parameterNames=array_flip($parameterNames[1]);

        return array_intersect_key($matches,$parameterNames);
    }

    /**
     * @param $uri
     * @param $method
     * @return void
     * @throws AppException
     * @throws AuthenticationException
     * @throws NotFoundException
     */
    public function direct($uri, $method)
    {
        foreach($this->routes[$method] as $route=>$routeData){
            $controller= $routeData['controller'];
            $minRole=$routeData['role'];

            $urlRule= $this->prepareRoute($route);

            if(preg_match($urlRule,$uri,$matches)===1){

                if(Security::isUserGranted($minRole)===false) {
                        if(!is_null(App::get('appUser'))){
                            throw new AuthenticationException('Acceso no autorizado');
                        }else{
                            $this->redirect('login');
                        }

                }else{

                        $parameters=$this->getParametersRoute($route,$matches);

                        list($controller,$action)=explode('@',$controller);

                        if($this->callAction($controller,$action,$parameters)===true){
                            return;
                        }
                    }
                }

            }


        throw new NotFoundException("No se ha definido una ruta para esta URI");


    }

    public function redirect( $path)
    {
        header('location: /' . $path);
        exit();
    }



}