<?php

use cursophp7\app\exception\AppException;
use cursophp7\app\exception\NotFoundException;
use cursophp7\core\App;
use cursophp7\core\Request;


try{
    require __DIR__.'/../core/bootstrap.php';
    /*Require??*/  App::get('router')->direct(Request::uri(),Request::method());

}
catch(AppException $appException){

   $appException->handleError();
}
